# MenuTitle: Hello world

__doc__ = """\
    Prints Hello world in Macro Window output. Just for testing purposes.
"""

from vanilla.dialogs import message


print("Hello world")
message("Hello world\n\nvanilla module is working properly")

Glyphs.showMacroWindow()
