# MenuTitle: Deletes all guides in selected layer glyphs.
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

__doc__="""
Deletes all guides in selected layer glyphs.
"""

# ---------------------
# Modules
# ---------------------
from GlyphsApp import GSGuideLine

# Getting the current font and selected layers
f = Font
layers = f.selectedLayers

# Iterating through all selected layers and deleting local guides
for l in layers:
	l.guides = []

# ---------------------
# Final
# ---------------------
Glyphs.showNotification("Guidelines", "All local guides have been deleted in current selection's layer")
