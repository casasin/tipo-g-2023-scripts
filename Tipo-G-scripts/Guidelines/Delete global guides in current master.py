# MenuTitle: Deletes all global guides in current master
# -*- coding: utf-8 -*-

__doc__="""
Deletes all global guides in current master.
"""

# ---------------------
# Modules
# ---------------------
from GlyphsApp import GSGuideLine

f = Font
m = f.selectedFontMaster

m.guides = []

# ---------------------
# Final
# ---------------------
Glyphs.showNotification("Guidelines", "All guidelines have been deleted")
