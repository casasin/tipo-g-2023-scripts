# Tipo-G 2023 scripts

---

A set of **Glyphs 3** scripts for students usage.

## Installation

This is the way to install and keep automatically updated our scripts, you'll need to do it only once:
- Go to *Glyphs 3* "Preferences/Addons"
- In the field "Alternate Plugin Repos" paste the following URL: https://casasin.com/tipog/TipoG2023-scripts.plist
- Close the "Preferences" panel.

If you don't have "Python version: 3.9.1 (Glyphs)" option available you need to install that Python Glyphs:
- Go to: "Window/Plugin Manager", click in the "Modules" button and INSTALL Python, FontTools and Vanilla.
- Go to (if you're not there already): "Window/Plugin Manager", click in the "Scripts" button and type in the search "Tipo-G 2023" to filter available scripts packages and find ours.
- **INSTALL** them and you should be ready to go. If you don't see the folder yet in the "Script" menu you might restart Glyphs and should be there.

Any future updates and changes will magically be updated in your app :-)

Let us know if you have any problem installing the scripts and We'll try to help.

Feel free to ask for any missing script to update and/or request something you think it might be useful for your workflow. We'll see if we can manage.

---

NO WARRANTY
